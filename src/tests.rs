
use bnf::Grammar;
use ndarray::{prelude::*, Array};

use crate::{
    differentiation::differentiate,
    calculator::{SpaceVector, parse, SOL_SPACE_SIZE},
    generation::GeneticGrammar,
    BNF_GRAMMAR,
};

pub fn test_example_chromosome() {
    let grammar: Grammar = BNF_GRAMMAR.parse().unwrap();

    // test the TL example chromosome:
    println!("Printing parsing of test chromosome. Evaluation should result in 'log(x*1*x)'.");
    // actual chromosome:
    //let a = array![[16, 3, 7, 4, 10, 28, 24, 1, 2, 4]];
    // for our modified grammar:
    let a = array![[12, 3, 5, 4, 0, 10, 25, 23, 1, 2, 4, 0]];
    // re. modified grammar: It appears that the original grammar is some 30% faster. Consider actually using it.
    for chromosome in a.axis_iter(Axis(0)) {
        let out = grammar.expression_from_chromosome(&chromosome);
        match out {
            Ok(s) => println!("TL example chromosome was evaluated to: {} (depth: {})", s.0, s.1),
            Err(e) => println!("TL example chromosome failed to evaluate,\n following error was thrown: {}", e.0),
        };
    }
    println!("");
}

pub fn test_example_solution() {
    let grammar: Grammar = BNF_GRAMMAR.parse().unwrap();
    // Manually test solution to example DE used
    println!("Printing parsing of actual solution chromosome. Score should result in something << 1e-7.");
    let v: Vec<i32> = vec![0, 2, 2, 0, 3, 0, 1, 0, 4, 0, 3, 3, 5, 2, 2, 0, 4, 0];
    let mut a = Array::<i32, _>::zeros((2,18));
    for (i, vi) in v.iter().enumerate() {
        a[[0,i]] = *vi;
    }
    let v: Vec<i32> = vec![0, 2, 0, 4, 0, 3, 2, 2, 0, 4, 0, 3, 3, 5, 5, 5, 5, 5]; // four last are dead
    for (i, vi) in v.iter().enumerate() {
        a[[1,i]] = *vi;
    }
    for chromosome in a.axis_iter(Axis(0)) {
        let out = grammar.expression_from_chromosome(&chromosome);
        let (expr, _) = match out {
            Ok(s) => {
                println!("Solution expression evaluated to: {} (depth: {})", s.0, s.1);
                s
            },
            Err(e) => {
                println!("Solution expression failed to evaluate,\n following error was thrown: {}", e.0);
                ("".to_string(), 100)
            },
        };
        let dexpr = differentiate(&expr, "x");
        let de = format!("({yp}) + 1/5*({y})-exp(0-x/5)*cos(x)", yp=dexpr, y=expr);
        let bc = format!("{}-0", y=expr);
        let result = parse(&de, &SpaceVector::linspace(0.0, 1.0, SOL_SPACE_SIZE));
        let mut score = result.iter().map(|e| {e.powf(2.0)}).sum::<f64>().sqrt();
        score += parse(&bc, &SpaceVector::linspace(0.0, 0.0, 1)).iter().map(|e| {e.powf(2.0)}).sum::<f64>().sqrt();
        println!("Score for solution expression: {:10.3e}", score);
    }
    println!("");
}
