use ndarray::{prelude::*, Array, OwnedRepr};
use ndarray_rand::RandomExt;
use bnf::Grammar;
use rand::{distributions::Uniform, thread_rng, Rng};
use std::{
    time,
    thread,
    ops::Range,
};
use crossbeam::channel;
use std::collections::HashSet;

use crate::{
    differentiation::differentiate,
    calculator::{SpaceVector, parse, SOL_SPACE_SIZE},
    generation::GeneticGrammar,
    BNF_GRAMMAR,
};

type Chromosomes = ArrayBase<OwnedRepr<i32>, Dim<[usize; 2]>>;
type Expressions = ArrayBase<OwnedRepr<String>, Dim<[usize; 1]>>;
type Fitness = ArrayBase<OwnedRepr<f64>, Dim<[usize; 1]>>;
type Sizes = ArrayBase<OwnedRepr<usize>, Dim<[usize; 1]>>;

struct DEOpts {
    mutation_rate: f64,
    replication_rate: f64,
    x_vector: SpaceVector,
    bc_penalty: f64,
    tournament_size: usize,
}

impl DEOpts {
    pub fn default() -> Self {
        DEOpts {
            mutation_rate: 0.1,
            replication_rate: 0.1,
            x_vector: {
                let mut rng = thread_rng();
                let (start, end) = (0.0, 10.0);
                let mut x_vec = SpaceVector::linspace(start, end, SOL_SPACE_SIZE);
                for x in x_vec.iter_mut() {
                    *x += (rng.gen::<f64>() - 0.5)/((end-start)*((SOL_SPACE_SIZE+1) as f64))
                }
                x_vec
            },
            bc_penalty: 100.0,
            tournament_size: 5,
        }
    }
}

struct Population {
    grammar: Grammar,
    options: DEOpts,
    stalling: usize,
    stalled: bool,
    // current data
    chromosomes: Chromosomes,
    depths: Sizes,
    expressions: Expressions,
    diff_expressions: Expressions,
    fitness: Fitness,
    // next step data
    new_chromosomes: Chromosomes,
    new_depths: Sizes,
    new_expressions: Expressions,
    new_diff_expressions: Expressions,
    new_fitness: Fitness,
}

impl Population {
    fn new() -> Self {
        let mut pop = Population {
            grammar: BNF_GRAMMAR.parse().unwrap(),
            options: DEOpts::default(),
            stalling: 0,
            stalled: false,
            chromosomes: Array::random((1024, 100), Uniform::from(0..256)),
            depths: Array::from_elem(1024, 0),
            expressions: Array::from_elem(1024, "".to_string()),
            diff_expressions: Array::from_elem(1024, "".to_string()),
            fitness: Array::from_elem(1024, f64::INFINITY),
            new_chromosomes: Array::random((1024, 100), Uniform::from(0..256)),
            new_depths: Array::from_elem(1024, 0),
            new_expressions: Array::from_elem(1024, "".to_string()),
            new_diff_expressions: Array::from_elem(1024, "".to_string()),
            new_fitness: Array::from_elem(1024, f64::INFINITY),
        };
        pop.init_all();
        pop.transfer();
        pop
    }
    fn init_all(&mut self) {
        for (i, chromosome) in self.new_chromosomes.axis_iter(Axis(0)).enumerate() {
            (self.new_expressions[i], self.new_depths[i]) = match self.grammar.expression_from_chromosome(&chromosome) {
                Ok(s) => s,
                Err(_) => ("".to_string(), 100)
            };
            // TODO: Undo the hard-coded DE & variable...
            self.new_diff_expressions[i] = differentiate(&self.new_expressions[i], "x");
            let de = format!("({yp}) + 1/5*({y})-exp(0-x/5)*cos(x)", yp=self.new_diff_expressions[i], y=self.new_expressions[i]);
            let bc = format!("{}-0", y=self.new_expressions[i]);
            self.new_fitness[i] = parse(&de, &self.options.x_vector).iter().map(|e| {e.powf(2.0)}).sum::<f64>().sqrt();
            self.new_fitness[i] += self.options.bc_penalty*parse(&bc, &SpaceVector::linspace(0.0, 0.0, 1)).iter().map(|e| {e.powf(2.0)}).sum::<f64>().sqrt();
            if self.new_fitness[i].is_nan() {
                self.new_fitness[i] = f64::INFINITY;
            }
        }
        self.sort();
        self.transfer();
    }
    fn sort(&mut self) {
        let indices = self.argsort();
        self.new_chromosomes = self.new_chromosomes.select(Axis(0), indices.as_slice());
        self.new_expressions = self.new_expressions.select(Axis(0), indices.as_slice());
        self.new_diff_expressions = self.new_diff_expressions.select(Axis(0), indices.as_slice());
        self.new_fitness = self.new_fitness.select(Axis(0), indices.as_slice());
    }
    fn argsort(&self) -> Vec<usize> {
        let mut indices = self.new_fitness.iter()
            .enumerate()
            .collect::<Vec<(usize, &f64)>>();
        indices.sort_by(|this, that| { this.1.total_cmp(that.1) });
        let indices = indices.iter()
            .map(|x| x.0)
            .collect::<Vec<usize>>();
        indices
    }
    fn mutate(&mut self, indices: Range<usize>) {
        let mut rng = thread_rng();
        for i in indices {
            let mut mutated = false;
            for (j, c) in self.new_chromosomes.slice_mut(s![i, ..]).iter_mut().enumerate() {
                if rng.gen::<f64>() < self.options.mutation_rate {
                    *c = rng.gen_range(0..256);
                    if j < self.new_depths[i] {
                        mutated = true;
                    }
                }
            }
            if !mutated {
                let j = rng.gen_range(0..self.new_depths[i]);
                self.new_chromosomes[[i,j]] = rng.gen_range(0..256);
            }
            self.update_new(&i)
        }
    }
    fn crossover(&mut self, indices: Range<usize>) {
        let mut rng = thread_rng();
        for (i, parent_pair) in self.full_tournament(indices).iter().enumerate() {
            let cut = rng.gen_range(
                1..self.depths[parent_pair.0].min(self.depths[parent_pair.1])
            );
            let replacing = self.new_chromosomes.shape()[0]-(i+1);
            for j in 0..self.chromosomes.shape()[1] {
                self.new_chromosomes[[replacing,j]] = self.chromosomes[
                        [if j < cut {parent_pair.0} else {parent_pair.1},j]
                    ];
            }
            self.update_new(&replacing);
        }
    }
    fn full_tournament(&self, indices: Range<usize>) -> HashSet<(usize, usize)> {
        let mut parents = HashSet::new();
        while parents.len() < indices.len() {
            let parent1 = self.tournament(&indices);
            let mut parent2 = self.tournament(&indices);
            while parent1 == parent2 {
                parent2 = self.tournament(&indices)
            }
            parents.insert((parent1, parent2));
        }
        parents
    }
    fn tournament(&self, indices: &Range<usize>) -> usize {
        let mut rng = thread_rng();
        let mut min = indices.end;
        for _ in 0..self.options.tournament_size {
            min = min.min(rng.gen_range(indices.clone()))
        }
        min
    }
    fn update_new(&mut self, i: &usize) {
        (self.new_expressions[*i], self.new_depths[*i]) = match self.grammar.expression_from_chromosome(&self.new_chromosomes.slice(s![*i,..])) {
            Ok(s) => s,
            Err(_) => ("".to_string(), 100)
        };
        // TODO: Undo the hard-coded DE & variable...
        self.new_diff_expressions[*i] = differentiate(&self.new_expressions[*i], "x");
        let de = format!("({yp}) + 1/5*({y})-exp(0-x/5)*cos(x)", yp=self.new_diff_expressions[*i], y=self.new_expressions[*i]);
        let bc = format!("{}-0", y=self.new_expressions[*i]);
        self.new_fitness[*i] = parse(&de, &self.options.x_vector).iter().map(|e| {e.powf(2.0)}).sum::<f64>().sqrt();
        self.new_fitness[*i] += self.options.bc_penalty*parse(&bc, &SpaceVector::linspace(0.0, 0.0, 1)).iter().map(|e| {e.powf(2.0)}).sum::<f64>().sqrt();
        if self.new_fitness[*i].is_nan() {
            self.new_fitness[*i] = f64::INFINITY;
        }
    }
    fn transfer(&mut self) {
        if self.expressions == self.new_expressions {
            self.stalling += 1;
            if self.stalling == 11 {
                self.stalled = true;
                println!("Population stalled. Has not changed for {} iterations.", self.stalling);
            }
        } else if self.stalling > 0 {
            self.stalling = 0;
        }
        self.chromosomes.assign(&self.new_chromosomes);
        self.depths.assign(&self.new_depths);
        self.expressions.assign(&self.new_expressions);
        self.diff_expressions.assign(&self.new_diff_expressions);
        self.fitness.assign(&self.new_fitness);
    }
    fn move_last(&mut self, i: &usize) {
        let mut m = self.fitness.len()-1;
        for n in 0..*i {
            for k in 0..self.chromosomes.shape()[1] {
                self.new_chromosomes[[m, k]] = self.chromosomes[[n, k]];
            }
            self.new_depths[m] = self.new_depths[n];
            self.new_expressions[m] = self.expressions[n].clone();
            self.new_diff_expressions[m] = self.diff_expressions[n].clone();
            self.new_fitness[m] = self.fitness[n];
            m -= 1;
        }
    }
    fn move_best(&mut self) {
        let mut i = 0;
        while self.fitness[i] < self.new_fitness[self.new_fitness.len()-(1+i)] {
            i += 1;
        }
        self.move_last(&i);
        self.sort();
    }
    fn take_mutate_all_step(&mut self) {
        self.mutate(0..1024);
        self.sort();
        self.move_best();
        self.transfer();
    }
    fn crossover_and_mutate_step(&mut self) {
        let non_replicants = (((self.options.replication_rate)*1024.0) as usize)..1024;
        self.crossover(non_replicants.clone());
        if self.new_fitness.iter().filter(|&e| e < &1e-7).count() > 0 {
            self.sort();
            self.transfer();
            return ()
        }
        self.mutate(non_replicants);
        self.sort();
        self.transfer();
    }
    fn differential_evolution(&mut self) {
        let mut rng = thread_rng();
        for i in 0..1024 {
            let mut a = rng.gen_range(0..1024);
            while a == i {
                a = rng.gen_range(0..1024);
            }
            let mut b = rng.gen_range(0..1024);
            while a == b || b == i {
                b = rng.gen_range(0..1024);
            }
            let mut c = rng.gen_range(0..1024);
            while a == b || c == b || c == i {
                c = rng.gen_range(0..1024);
            }
            let r = rng.gen_range(0..self.depths[i]);
            for j in 0..100 {
                self.new_chromosomes[[i,j]] = if rng.gen::<f64>() < 0.9 || r == i {
                    self.chromosomes[[a,j]]
                        + (self.chromosomes[[b, j]] - self.chromosomes[[c, j]]) * (if rng.gen::<f64>() < 0.9 {1} else {2})
                } else {
                    self.chromosomes[[i,j]]
                };
                while self.new_chromosomes[[i,j]] > 255 {
                    self.new_chromosomes[[i,j]] -= 255
                }
                while self.new_chromosomes[[i,j]] < 0 {
                    self.new_chromosomes[[i,j]] += 255
                }
            }
            self.update_new(&i)
        }
    }
    fn move_better(&mut self) {
        for i in 0..1024 {
            if self.new_fitness[i] > self.fitness[i] {
                self.new_chromosomes.slice_mut(s![i,..]).assign(&self.chromosomes.slice(s![i,..]));
                self.new_depths.slice_mut(s![i]).assign(&self.depths.slice(s![i]));
                self.new_expressions.slice_mut(s![i]).assign(&self.expressions.slice(s![i]));
                self.new_diff_expressions.slice_mut(s![i]).assign(&self.diff_expressions.slice(s![i]));
                self.new_fitness.slice_mut(s![i]).assign(&self.fitness.slice(s![i]));
            }
        }
    }
    fn differential_evolution_step(&mut self) {
        self.differential_evolution();
        self.move_better();
        self.sort();
        self.transfer();
    }
}

fn random_best() -> (String, f64) {
    let mut best_score = f64::INFINITY;
    let mut best_expr = "".to_string();
    let pop = Population::new();
    for (i, fit) in pop.fitness.iter().enumerate() {
        if fit < &best_score {
            best_score = *fit;
            best_expr = pop.expressions[i].clone();
        }
    }
    (best_expr, best_score)
}

pub fn faking_it() {
    // just to kill warnings...
    random_best();
    let mut pop = Population::new();
    pop.take_mutate_all_step();
    pop.crossover_and_mutate_step();
    pop.differential_evolution_step();
}

pub fn pop_search() {
    let mut then = time::Instant::now();

    let mut pop_vec = vec![
        Population::new(), Population::new(), Population::new(), Population::new(),
        Population::new(), Population::new(), Population::new(), //Population::new(),
    ];
    let n_procs = pop_vec.len();

    let (mut best_score, mut best_expr) = pop_vec.iter()
        .map(|pop| {(pop.fitness[0], pop.expressions[0].clone())})
        .max_by(|this, that| this.0.total_cmp(&that.0))
        .unwrap();

        println!("Starting best: {:10.3e}:\n {}", best_score, best_expr);

    let mut counter = 0;

    'solve: loop {
        let (tx, rx) = channel::unbounded();

        for _ in 0..n_procs {
            let tx0 = tx.clone();
            let mut pop = pop_vec.pop().unwrap();
            thread::spawn(move || {
                //random_best()
                //pop.take_mutate_all_step();
                //pop.crossover_and_mutate_step();
                pop.differential_evolution_step();
                tx0.send(pop).unwrap();
            });
        }

        std::mem::drop(tx); // all senders except the original `tx` will be dropped, so drop manually
        for pop in &rx {
            pop_vec.push(pop)
        }

        let (new_best_score, new_best_expr) = pop_vec.iter()
            .map(|pop| {(pop.fitness[0], pop.expressions[0].clone())})
            .max_by(|this, that| this.0.total_cmp(&that.0))
            .unwrap();

        if new_best_score < best_score {
            best_score = new_best_score;
            best_expr = new_best_expr;
            println!("Found better solution {:10.3e}:\n {}", best_score, best_expr);
            if best_score < 1e-7 {
                break 'solve;
            }
        }

        let t = time::Instant::now() - then;
        then = time::Instant::now();
        if counter % 10 == 0 {
            println!("Time: {:.3}; time per batch: {:.3} ({})", t.as_secs_f64(), t.as_secs_f64()/(n_procs as f64), counter);
        }
        counter += 1;
    }
}
