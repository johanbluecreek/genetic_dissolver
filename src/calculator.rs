use ndarray::Array1;
use lazy_static::lazy_static;
use pest::{
    prec_climber::*,
    iterators::{Pair, Pairs},
    Parser,
};
use pest_derive::Parser;

pub type SpaceVector = Array1<f64>;

pub const SOL_SPACE_SIZE: usize = 100;
const SOL_SPACE_SHAPE: (usize,) = (SOL_SPACE_SIZE,);

#[derive(Parser)]
#[grammar = "grammars/calculator.pest"]
struct Calculator;

lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use Assoc::*;
        use Rule::*;

        PrecClimber::new(vec![
            Operator::new(add, Left) | Operator::new(subtract, Left),
            Operator::new(multiply, Left) | Operator::new(divide, Left),
        ])
    };
}

fn eval(expression: Pairs<Rule>, x_vec: &SpaceVector) -> SpaceVector {
    PREC_CLIMBER.climb(
        expression,
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::function => {
                let mut i = pair.into_inner();
                let name = i.next().unwrap().as_str();
                let value = eval(i, x_vec);
                apply_fun(name, value)
            }
            Rule::digit => {
                SpaceVector::from_elem(SOL_SPACE_SIZE,pair.as_str().trim().parse::<f64>().unwrap())
            },
            Rule::expr => {
                eval(pair.into_inner(), x_vec)
            },
            Rule::vars => {
                if pair.as_str() == "x" {
                    x_vec.clone()
                } else {
                    SpaceVector::from_elem(SOL_SPACE_SHAPE, f64::NAN)
                }
            },
            _ => SpaceVector::from_elem(SOL_SPACE_SHAPE, f64::NAN),
        },
        |lhs: SpaceVector, op: Pair<Rule>, rhs: SpaceVector| match op.as_rule() {
            Rule::add => lhs + rhs,
            Rule::subtract => lhs - rhs,
            Rule::multiply => lhs * rhs,
            Rule::divide => lhs / rhs,
            _ => SpaceVector::from_elem(SOL_SPACE_SHAPE, f64::NAN),
        },
    )
}

fn apply_fun(name: &str, arg: SpaceVector) -> SpaceVector {
    match name {
        "sin" => arg.map(|e| e.sin()),
        "cos" => arg.map(|e| e.cos()),
        "log" => arg.map(|e| e.ln()),
        "exp" => arg.map(|e| e.exp()),
        _ => SpaceVector::from_elem(SOL_SPACE_SHAPE, f64::NAN),
    }
}

pub fn parse(input: &str, vec_x: &SpaceVector) -> SpaceVector {
    let parse_result = Calculator::parse(Rule::calculation, input);
    match parse_result {
        Ok(r) => eval(r, &vec_x),
        Err(_) => SpaceVector::from_elem(SOL_SPACE_SHAPE, f64::NAN),
    }
}
