
use ndarray::{prelude::*, ViewRepr};
use bnf::{Grammar,Expression,Term};

pub trait GeneticGrammar {
    fn expression_from_chromosome(&self, chromosome: &ArrayBase<ViewRepr<&i32>, Dim<[usize; 1]>>) -> Result<(String, usize), (String, usize)>;
    fn traverse_vec(&self, rule: &str, chromosome: &ArrayBase<ViewRepr<&i32>, Dim<[usize; 1]>>, i: &mut usize) -> Result<(String, usize), (String, usize)>;
    fn eval_terminal_vec(&self, term: &Term, chromosome: &ArrayBase<ViewRepr<&i32>, Dim<[usize; 1]>>, i: &mut usize) -> Result<(String, usize), (String, usize)>;
}

impl GeneticGrammar for Grammar {
    fn expression_from_chromosome(&self, chromosome: &ArrayBase<ViewRepr<&i32>, Dim<[usize; 1]>>) -> Result<(String, usize), (String, usize)> {
        return self.traverse_vec("expr", chromosome, &mut 0);
    }
    fn traverse_vec(&self, rule: &str, chromosome: &ArrayBase<ViewRepr<&i32>, Dim<[usize; 1]>>, i: &mut usize) -> Result<(String, usize), (String, usize)> {
        loop {
            let nonterm = Term::Nonterminal(rule.to_string());
            let find_lhs = self.productions_iter().find(|&x| x.lhs == nonterm);

            let production = match find_lhs {
                Some(p) => p,
                None => return Ok((nonterm.to_string(), *i))
            };

            let expressions = production.rhs_iter().collect::<Vec<&Expression>>();
            if *i < chromosome.len() {
                *i += 1;
            } else {
                return Err(("Chromosome not long enough.".to_string(), *i));
            }
            let expression = expressions[(chromosome[*i-1] % (expressions.len() as i32)) as usize];

            let mut result = String::new();
            for term in expression.terms_iter() {
                match self.eval_terminal_vec(term, &chromosome, i) {
                    Ok(s) => result.push_str(&s.0),
                    Err(e) => return Err((e.0, *i)),
                }
            }

            return Ok((result, *i));
        }
    }
    fn eval_terminal_vec(&self, term: &Term, chromosome: &ArrayBase<ViewRepr<&i32>, Dim<[usize; 1]>>, i: &mut usize) -> Result<(String, usize), (String, usize)> {
        match *term {
            Term::Nonterminal(ref nt) => self.traverse_vec(nt, chromosome, i),
            Term::Terminal(ref t) => Ok((t.clone(), *i)),
        }
    }
}
