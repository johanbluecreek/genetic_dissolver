
use pest::{
    prec_climber::*,
    iterators::{Pair, Pairs},
    Parser,
};
use pest_derive::Parser;
use lazy_static::lazy_static;

#[derive(Parser)]
#[grammar = "grammars/calculator.pest"]
struct Calculator;

type Stacks = (String, String);

lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use Assoc::*;
        use Rule::*;

        PrecClimber::new(vec![
            Operator::new(add, Left) | Operator::new(subtract, Left),
            Operator::new(multiply, Left) | Operator::new(divide, Left),
        ])
    };
}

fn diff_fun(name: &str, arg: Stacks) -> Stacks {
    match name {
        "sin" => (
            if arg.0 == "0" {
                "0".to_string()
            } else {
                format!("sin({})", arg.0)
            },
            if arg.1 == "0" {
                "0".to_string()
            } else if arg.1 == "1" {
                format!("cos({})", arg.0)
            } else {
                format!("cos({})*({})", arg.0, arg.1)
            }
        ),
        "cos" => (
            if arg.0 == "0" {
                "1".to_string()
            } else {
                format!("cos({})", arg.0)
            },
            if arg.1 == "0" {
                "0".to_string()
            } else if arg.1 == "1" {
                format!("0-sin({})", arg.0)
            } else {
                format!("0-sin({})*({})", arg.0, arg.1)
            }
        ),
        "log" => (
            if arg.0 == "1" {
                "0".to_string()
            } else {
                format!("log({})", arg.0)
            },
            if arg.1 == "0" {
                "0".to_string()
            } else if arg.1 == "1" {
                format!("1/({})", arg.0)
            } else {
                format!("1/({})*({})", arg.0, arg.1)
            }
        ),
        "exp" => (
            if arg.0 == "0" {
                "1".to_string()
            } else {
                format!("exp({})", arg.0)
            },
            if arg.1 == "0" {
                "0".to_string()
            } else if arg.1 == "1" {
                format!("exp({})", arg.0)
            } else {
                format!("exp({})*({})", arg.0, arg.1)
            }
        ),
        _ => ("0".to_string(), "0".to_string()),
    }
}

fn diff(expression: Pairs<Rule>, v: &str) -> Stacks {
    PREC_CLIMBER.climb(
        expression,
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::function => {
                let mut i = pair.into_inner();
                let name = i.next().unwrap().as_str();
                let arg = diff(i, v);
                diff_fun(name, arg)
            }
            Rule::digit => {
                (pair.as_str().to_string(), "0".to_string())
            },
            Rule::expr => diff(pair.into_inner(), v),
            Rule::vars => {
                if pair.as_str() == v {
                    (pair.as_str().to_string(), "1".to_string())
                } else {
                    ("0".to_string(), "0".to_string())
                }
            },
            _ => {
                ("log(0)".to_string(), "log(0)".to_string())
            },
        },
        |lhs: Stacks, op: Pair<Rule>, rhs: Stacks| match op.as_rule() {
            Rule::add => {
                (
                    if lhs.0 == "0" {
                        rhs.0
                    } else if rhs.0 == "0" {
                        lhs.0
                    } else {
                        format!("{}+{}", lhs.0, rhs.0)
                    },
                    if lhs.1 == "0" {
                        rhs.1
                    } else if rhs.1 == "0" {
                        lhs.1
                    } else {
                        format!("{}+{}", lhs.1, rhs.1)
                    }
                )
            },
            Rule::subtract => {
                (
                    if rhs.0 == "0" {
                        format!("{}", lhs.0)
                    } else {
                        format!("{}-({})", lhs.0, rhs.0)
                    },
                    if rhs.1 == "0" {
                        format!("{}", lhs.1)
                    } else {
                        format!("{}-({})", lhs.1, rhs.1)
                    }
                )
            },
            Rule::multiply => {
                (
                    if lhs.0 == "0" || rhs.0 == "0" {
                        "0".to_string()
                    } else if lhs.0 == "1" {
                        format!("{}", rhs.0)
                    } else if rhs.0 == "1" {
                        format!("{}", lhs.0)
                    } else {
                        format!("({})*({})", lhs.0, rhs.0)
                    },
                    if lhs.0 == "0" || rhs.0 == "0" {
                        "0".to_string()
                    } else if lhs.0 == "1" {
                        format!("{}", rhs.1)
                    } else if rhs.0 == "1" {
                        format!("{}", lhs.1)
                    } else {
                        format!("({l})*({dr})+({dl})*({r})", l=lhs.0, r=rhs.0, dl=lhs.1, dr=rhs.1)
                    }
                )
            },
            Rule::divide => {
                if rhs.0 == "0" {
                    ("log(0)".to_string(), "log(0)".to_string())
                } else {
                    (
                        format!("({})/({})", lhs.0, rhs.0),
                        if rhs.1 == "0" {
                            if rhs.0 == "1" {
                                lhs.1
                            } else {
                                format!("({dl})/({r})", r=rhs.0, dl=lhs.1)
                            }
                        } else {
                            format!("(({dl})/({r})-(({l})*({dr}))/(({r})*({r})))", l=lhs.0, r=rhs.0, dl=lhs.1, dr=rhs.1)
                        }
                    )
                }
            },
            _ => ("log(0)".to_string(),"log(0)".to_string()),
        },
    )
}

pub fn differentiate(input: &str, v: &str) -> String {
    let parse_result = Calculator::parse(Rule::calculation, input);
    match parse_result {
        Ok(r) => diff(r, v).1,
        Err(_) => "".to_string(),
    }
}
