use genetic_dissolver::{
    solvers::{
        pop_search,
    },
    tests::{test_example_chromosome, test_example_solution},
};
use std::time;

fn main() {
    test_example_chromosome();
    test_example_solution();

    // do search
    println!("Performing actual search...");
    let start = time::Instant::now();
    pop_search();
    let t = time::Instant::now() - start;
    println!("Took: {}", t.as_secs_f64());
}
