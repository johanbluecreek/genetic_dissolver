pub mod differentiation;
pub mod generation;
pub mod calculator;
pub mod solvers;
pub mod tests;

pub const BNF_GRAMMAR: &str = std::include_str!("./grammars/tl_grammar.bnf");
